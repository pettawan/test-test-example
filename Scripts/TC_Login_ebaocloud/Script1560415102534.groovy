import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')
WebUI.navigateToUrl('https://sandbox.eu.ebaocloud.com/#/main')
WebUI.maximizeWindow()

String username = "eBao.EUUAT"
String password = "eBao123!"

WebUI.verifyElementVisible(findTestObject('ebaocloud/link_SignIn'),FailureHandling.STOP_ON_FAILURE)
WebUI.click(findTestObject('ebaocloud/link_SignIn'))
WebUI.delay(3)

WebUI.verifyElementVisible(findTestObject('ebaocloud/input_username'),FailureHandling.STOP_ON_FAILURE)
WebUI.setText(findTestObject('ebaocloud/input_username'), username)
WebUI.delay(1)
WebUI.setText(findTestObject('ebaocloud/input_password'), password)
WebUI.delay(1)
WebUI.click(findTestObject('ebaocloud/btn_submit'))
WebUI.delay(3)


WebUI.verifyElementVisible(findTestObject('ebaocloud/text_title_name'),FailureHandling.STOP_ON_FAILURE)

text_title_name = WebUI.getText(findTestObject('ebaocloud/text_title_name'))
println ""+text_title_name

WebUI.verifyMatch(text_title_name, username, false)
WebUI.delay(1)

WebUI.verifyElementVisible(findTestObject('ebaocloud/text_title_name'),FailureHandling.STOP_ON_FAILURE)
WebUI.click(findTestObject('ebaocloud/text_title_name'))
WebUI.delay(1)
WebUI.click(findTestObject('ebaocloud/sign_out'))
WebUI.delay(2)
WebUI.verifyElementVisible(findTestObject('ebaocloud/link_SignIn'),FailureHandling.STOP_ON_FAILURE)

WebUI.closeBrowser()





