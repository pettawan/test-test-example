import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

def currentOdometerReading = '11000'
def previousOdometerReading = '10000'
def gasAddedtotheTank = '40'
def gasPrice = '4.5'
def result = '30.00 miles per gallon'

WebUI.openBrowser('')

WebUI.navigateToUrl('http://www.calculator.net/gas-mileage-calculator.html')

WebUI.maximizeWindow()

WebUI.waitForPageLoad(5)

'Input Current Odometer Reading'
WebUI.setText(findTestObject('Page_Gas Mileage Calculator/input_uscodreading'), currentOdometerReading)

'Input Previous Odometer Reading'
WebUI.setText(findTestObject('Page_Gas Mileage Calculator/input_uspodreading'), previousOdometerReading)

'Input Gas Added to the Tank'
WebUI.setText(findTestObject('Page_Gas Mileage Calculator/input_usgasputin'), gasAddedtotheTank)

'Input Gas Price'
WebUI.setText(findTestObject('Page_Gas Mileage Calculator/input_usgasprice'), gasPrice)

'Click Calculator Button'
WebUI.click(findTestObject('Page_Gas Mileage Calculator/input'))

WebUI.waitForPageLoad(5)

def getResult = WebUI.getText(findTestObject('Page_Gas Mileage Calculator/resultCal'))

if (result.equals(getResult)) {
	println('result ===' + 'Pass')
	WebUI.verifyMatch(getResult,result, false)
} else {
	println('result ===' + 'fail')
	WebUI.verifyMatch(getResult,result, false)
}


@com.kms.katalon.core.annotation.TearDown
@com.kms.katalon.core.annotation.TearDownIfFailed
@com.kms.katalon.core.annotation.TearDownIfError
def TearDown() {
	'Close Browser'
	WebUI.closeBrowser()
}

